/**
 * @private
 * @description Get first level object length
 *
 * @param {Object} object
 * @returns {Number}
 */
const objectLength = object => Object.keys(object).length

/**
 * @private
 * @description Serialize params to URL
 *
 * @param {string} [base='']
 * @param {*} params
 * @returns {String}
 */
const serialize = (base = '', params) => {
  return objectLength(params) === 0
    ? base
    : base +
        '?' +
        Object.keys(params)
          .map(function (k) {
            return encodeURIComponent(k) + '=' + encodeURIComponent(params[k])
          })
          .join('&')
}

/**
 * @private
 * @description It sends a HTTP request. It's normally called by its synonyms
 *
 * @param {string} [url='']
 * @param {*} [params={}]
 * @param {string} [method='GET']
 * @param {*} [content={}]
 * @returns {Promise}
 */
const fetcher = (url = '', params = {}, method = 'GET', content = {}) => {
  url = urlMaker(url, params)

  content.method = method

  if (objectLength(params) > 0 && method === 'GET' && url !== false) {
    url = serialize(url, params)
  }

  if (objectLength(params) > 0 && method !== 'GET') {
    content.body = JSON.stringify(params)

    if (content.headers == null) {
      content.headers = {}
    }

    if (content.headers['Content-Type'] == null) {
      content.headers['Content-Type'] = 'application/json'
    }
  }

  if (url !== false) {
    return fetch(url, content)
      .then(r => {
        if (r.status === 200 || r.status === 201) {
          return r.json()
        } else {
          let json = r.json()

          if (json.then != null) {
            json.then(data => {
              let message = data.message != null ? data.message : 'Error'

              console.log(message)
            })
          } else {
            console.log('Ha ocurrido un error ' + r.status)
          }

          return Promise.reject(new Error())
        }
      })
      .catch(e => {
        return Promise.reject(e)
      })
  } else {
    return Promise.reject(new Error('Bad params'))
  }
}

/**
 * @description It makes a GET request
 *
 * @param {string} [url=''] Base URL
 * @param {Object} [params={}] Params sendend in url
 * @param {Object} [optionalContent={}] Additionals headers
 * @returns {Promise}
 */
const get = (url = '', params = {}, optionalContent = {}) => fetcher(url, params, 'GET', optionalContent)

/**
 * @description It makes a POST request
 *
 * @param {string} [url=''] Base URL
 * @param {Object} [params={}] Params sendend in url and body
 * @param {Object} [optionalContent={}] Additionals headers
 * @returns {Promise}
 */
const post = (url = '', params = {}, optionalContent = {}) => fetcher(url, params, 'POST', optionalContent)

/**
 * @description It makes a PUT request
 *
 * @param {string} [url=''] Base URL
 * @param {Object} [params={}] Params sendend in url and body
 * @param {Object} [optionalContent={}] Additionals headers
 * @returns {Promise}
 */
const put = (url = '', params = {}, optionalContent = {}) => fetcher(url, params, 'PUT', optionalContent)

/**
 * @description It makes a DELETE request (delete is a reserved word so it will be erase)
 *
 * @param {string} [url=''] Base URL
 * @param {Object} [params={}] Params sendend in url and body
 * @param {Object} [optionalContent={}] Additionals headers
 * @returns {Promise}
 */
const erase = (url = '', params = {}, optionalContent = {}) => fetcher(url, params, 'DELETE', optionalContent)

/**
 * @private
 * @description Search and replace params in urls
 * @example
 * // returns /api/entity/1/name/myvalue
 * urlMaker('/api/entity/:id/name/:value', { id: 1, value: 'myvalue' })
 * @param {string} [url=''] Base url
 * @param {Object} params Params to replace
 * @returns {string} New URL
 */
const urlMaker = (url = '', params) => {
  const expression = /:[a-z]+\/?/gi
  let newUrl = url
  let matches = url.match(expression)

  if (matches != null) {
    matches.map(fullItem => {
      let item = fullItem.substring(1, fullItem.length).replace('/', '')
      if (newUrl !== false && params[item] != null) {
        newUrl = newUrl.replace(':' + item, params[item])
      } else {
        newUrl = false
      }
    })
  }

  return newUrl
}

window.services = {
  get,
  post,
  put,
  delete: erase
}
