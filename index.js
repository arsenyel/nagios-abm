const Restify = require('restify')
const server = Restify.createServer()
const path = require('path')
const fs = require('fs')

server.use(Restify.plugins.queryParser())
server.use(Restify.plugins.bodyParser({ mapParams: false }))
server.get('/*', Restify.plugins.serveStatic({
  directory: path.join(__dirname, './'),
  default: 'index.html'
}))

const urls = {
  list: '/api/list',
  add: '/api/add',
  edit: '/api/edit/:original',
  delete: '/api/delete/:original'
}

function readCurrentConfiguration () {
  let file = path.join(__dirname, './IGN_SVCS.cfg')
  let fileContent = fs.readFileSync(file, 'utf8')
  let hosts = fileContent.replace(/ {1,}/g, ' ').split('define host')

  let newHosts = []

  hosts.map(host => {
    let newHost = {}
    host.split('\n').map(item => {
      let [key, value] = item.trim().split(' ')

      switch (key) {
        case 'host_name':
          newHost.name = value
          break
        case 'address':
          newHost.ip = value
          break
        case 'hostgroups':
          newHost.group = value
          break
      }
    })

    if (newHost.name != null) {
      newHosts.push(newHost)
    }
  })

  newHosts.sort((a, b) => {
    if (a.name > b.name) {
      return 1
    }
    if (a.name < b.name) {
      return -1
    }
    return 0
  })
  return newHosts
}

function generateNewConfiguration (src, dst = './IGN_SVCS.cfg') {
  let file = path.join(__dirname, dst)
  try {
    let stream = fs.createWriteStream(file)
    let base = ''
    src.sort((a, b) => {
      if (a.name > b.name) {
        return 1
      }
      if (a.name < b.name) {
        return -1
      }
      return 0
    })
    src.map(item => {
      base += `
define host{
  use             generic-host
  host_name       ${item.name}
  alias           ${item.name}
  address         ${item.ip}
  parents         localhost
  hostgroups      ${item.group}
}
`
    })
    stream.write(base)
  } catch (err) {
    console.log(err.message)
  }
}

server.get(urls.list, (req, res) => {
  res.send(readCurrentConfiguration())
})

server.post(urls.add, (req, res) => {
  let base = readCurrentConfiguration()
  if (req.body.name != null && req.body.name.trim() !== '' &&
    req.body.ip != null && req.body.ip.trim() !== '' &&
    req.body.group != null && req.body.group.trim() !== ''
  ) {
    base.push({
      name: req.body.name,
      ip: req.body.ip,
      group: req.body.group
    })

    generateNewConfiguration(base)

    res.send(201, { message: 'created' })
  } else {
    res.send(400, { message: 'errors' })
  }
})

server.put(urls.edit, (req, res) => {
  let original = req.params.original
  let base = readCurrentConfiguration()
  if (req.body.name != null && req.body.name.trim() !== '' &&
    req.body.ip != null && req.body.ip.trim() !== '' &&
    req.body.group != null && req.body.group.trim() !== '' &&
    original != null && original.trim() !== ''
  ) {
    let index = base.findIndex(item => item.name === original)

    if (index !== -1) {
      base[index] = {
        name: req.body.name,
        ip: req.body.ip,
        group: req.body.group
      }
      generateNewConfiguration(base)

      res.send(200, { message: 'edited' })
    } else {
      res.send(404)
    }
  } else {
    res.send(400)
  }
})

server.del(urls.delete, (req, res) => {
  let original = req.params.original
  let base = readCurrentConfiguration()
  if (original != null && original.trim() !== '') {
    let index = base.findIndex(item => item.name === original)

    if (index !== -1) {
      delete base[index]
      generateNewConfiguration(base)

      res.send(200, { message: 'deleted' })
    } else {
      res.send(404)
    }
  } else {
    res.send(400)
  }
})

server.listen(8080, () => {
  console.log('%s listening at %s')
})
